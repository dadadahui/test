var bar1data = {
  x:['1月', '秀洲区', '嘉善县', '海盐县', '海宁市', '平湖市', '桐乡市', '经开区', '嘉兴港区'],
  data:[
    [2.0, 4.9, .7, 135.6, 162.2, 32.6, 20.0, 6.4, 1111],
    [2.6, 70.7, 175, 6.0, 2.3],
    [2.0, 2.2, 3.4, 23.0, 10, 6.2]
  ]
}
var gaugeData = {value: 22, name: '隐患整改率'};
showBar1(bar1data);
showGauge(gaugeData)
function showBar1(data){

  var chart = echarts.init(document.getElementById('bar1'));
  chart.setOption(
    {
      color: ['#1178C9', '#00A77B'],
      tooltip: {
        trigger: 'axis'
      },
      grid: {
        bottom:'15%',
        left:'12%',
        right: '3%'
      },
      legend: {
        show: true,
        top: 10,
        data: ['应排查企业数', '已排查企业数', '自查自报率'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      xAxis: [
        {
          type: 'category',
          data:data.x,
          axisPointer: {
            type: 'shadow'
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          name: '企业数量（单位：家）',
          nameLocation: 'middle',
          nameGap: '50',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          },
          nameTextStyle: {
            color: '#686868'
          }

        },

        {
          type: 'value',
          axisLabel: {
            formatter: '{value} %',
            color: '#686868'
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          }

        }
      ],
      series: [
        {
          name: '应排查企业数',
          type: 'bar',
          data: data.data[0]
        },
        {
          name: '已排查企业数',
          type: 'bar',
          data: data.data[1]
        },
        {
          name: '自查自报率',
          type: 'line',
          lineStyle: {
            normal: {
              color: '#FB0D00'
            }
          },
          yAxisIndex: 1,
          data: data.data[2]
        }
      ]
    }
  )
}

//隐患整改率
function showGauge(data) {
  var chart = echarts.init(document.getElementById('gauge'));
  chart.setOption(
    {
      series : [
        {
          name:'',
          type:'gauge',
          radius:'100%',
          center:['50%','55%'],
          detail : {
            formatter:'{value}%',
            fontSize:'12'
          },
          data:data,
          axisTick:{
            length:0
          },
          axisLabel: {
            show: true,
            distance: -20,
          },
          //刻度线
          axisLine: {
            show: false,
            distance: 10,
            lineStyle: {
              width: 8,
              color: [
                [0.2, '#E51C23'],
                [0.8, '#F5A623'],
                [1, '#0D80FE']
              ]
            }
          },
          splitLine:{
            show:false
          },
          pointer:{
            width:2
          },
          title:{
            color:'#63869E',
            fontSize:12
          }
        }
      ]
    }
  )
  $(window).resize(function(){
      chart.resize();
    }
  )
}