var bar1data = {
  x:['1月', '秀洲区', '嘉善县', '海盐县', '海宁市', '平湖市', '桐乡市', '经开区', '嘉兴港区'],
  data:
    [2.0, 4.9, .7, 135.6, 162.2, 32.6, 20.0, 6.4, 1111],

}
showBar1(bar1data);
function showBar1(data){

  var chart = echarts.init(document.getElementById('bar1'));
  chart.setOption(
    {
      color: ['#1178C9'],
      tooltip: {
        trigger: 'axis'
      },
      grid: {
        bottom:'5%',
        left:'3%',
        right: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data:data.x,
          axisPointer: {
            type: 'shadow'
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          name: '单位：个',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          },
          nameTextStyle: {
            color: '#686868'
          }

        }
      ],
      series: [
        {
          type: 'bar',
          data: data.data
        }
      ]
    }
  )
}

