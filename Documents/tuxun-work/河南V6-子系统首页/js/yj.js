var bar1data = {
  x:['1月', '秀洲区', '嘉善县', '海盐县', '海宁市', '平湖市', '桐乡市', '经开区', '嘉兴港区'],
  data:[
    [2.0, 4.9, .7, 135.6, 162.2, 32.6, 20.0, 6.4, 1111],
    [2.6, 70.7, 175, 6.0, 2.3],
    [2.0, 2.2, 3.4, 23.0, 10, 6.2]
  ]
}
showBar1(bar1data);
function showBar1(data){

  var chart = echarts.init(document.getElementById('bar1'));
  chart.setOption(
    {
      color: ['#1178C9', '#00A77B','#FF8516'],
      tooltip: {
        trigger: 'axis'
      },
      grid: {
        bottom:'15%',
        left:'10%',
        right: '3%'
      },
      legend: {
        show: true,
        top: 10,
        data: ['应急预案', '应急队伍', '应急演练'],
        textStyle: {
          color: '#6F7B8D'
        }
      },
      xAxis: [
        {
          type: 'category',
          data:data.x,
          axisPointer: {
            type: 'shadow'
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          name: '企业数量（单位：家）',
          nameLocation: 'middle',
          nameGap: '50',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          },
          nameTextStyle: {
            color: '#686868'
          }

        },

        {
          type: 'value',
          axisLabel: {
            formatter: '{value} %',
            color: '#686868'
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          }

        }
      ],
      series: [
        {
          name: '应急预案',
          type: 'bar',
          data: data.data[0]
        },
        {
          name: '应急队伍',
          type: 'bar',
          data: data.data[1]
        },
        {
          name: '应急演练',
          type: 'bar',
          data: data.data[2]
        }
      ]
    }
  )
}
