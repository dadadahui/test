var pie1Data = [
  {value: 51, name: '年度检查'},
  {value: 81, name: '月度检查'},
  {value: 15, name: '日常检查'},
  {value: 10, name: '事故调查'},
  {value: 10, name: '投诉举报检查'}
]
var pie2Data = [
  {value: 100, name: '危险化学品',code:'SYS1601'},
  {value: 1, name: '烟花爆竹',code:'SYS1602'},
  {value: 15, name: '非煤矿山',code:'SYS1603'},
  {value: 10, name: '冶金行业',code:'SYS1604'},
  {value: 110, name: '有色金属',code:'SYS1605'},
  {value: 110, name: '建材行业',code:'SYS1606'},
  {value: 110, name: '机械制造',code:'SYS1607'},
  {value: 110, name: '轻工业',code:'SYS1608'},
  {value: 110, name: '纺织工业',code:'SYS1609'},
  {value: 110, name: '烟草行业',code:'SYS1610'},
  {value: 110, name: '商贸行业',code:'SYS1611'},
  {value: 110, name: '其他',code:'SYS1699'}
]

var bar1data = {
  x:['1月', '秀洲区', '嘉善县', '海盐县', '海宁市', '平湖市', '桐乡市', '经开区', '嘉兴港区'],
  data:[2, 9,7, 135.6, 162, 32.6, 20, 6, 1111]
}
showPie1(pie1Data)
showPie2(pie2Data)
showBar1(bar1data);

function getColorByCodeOrName(code,name) {
  if (code == "SYS1601" || name == '危险化学品') {  // 危险化学品
    return "#FF4321";
  } else if (code == "SYS1602" || name == '烟花爆竹') {//烟花爆竹
    return "#B91D1C";
  } else if (code == "SYS1603" || name == '非煤矿山') {//非煤矿山
    return "#5C5F6B";
  } else if (code == "SYS1604" || name == '冶金行业') {//冶金行业
    return "#F99A1B";
  } else if (code == "SYS1605" || name == '有色金属') {//有色金属
    return "#7FA51D";
  } else if (code == "SYS1606" || name == '建材行业') {//建材行业
    return "#139AC2";
  } else if (code == "SYS1607" || name == '机械制造') {//机械制造
    return "#005D93";
  } else if (code == "SYS1608" || name == '轻工业') {//轻工业
    return "#48A325";
  } else if (code == "SYS1609" || name == '纺织工业') {//纺织工业
    return "#ED5A0D";
  } else if (code == "SYS1610" || name == '烟草行业') {//烟草行业
    return "#006C26";
  }  else if (code == "SYS1611" || name == '商贸行业') {//商贸行业
    return "#27962E";
  } else if(code == "SYS1699" || name == '其他') {//其他
    return "#5C5F6B";
  }else{
    return '#018DD3'
  }
}
function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))
  chart.setOption(
    {
      color: [
        '#0099FF',
        '#21D376',
        '#F5A623',
        '#EB3432',
        '#A4BCC6'
      ],
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name:'接触职业病危害因素占比分布',
          type: 'pie',
          radius: [25, 45],
          center: ['50%', '50%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1'
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}

function showPie2 (data) {
  var chart = echarts.init(document.getElementById('pie2'))

  chart.setOption(
    {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name:'危害因素的行业分布情况',
          type: 'pie',
          radius: [25, 45],
          center: ['50%', '50%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          labelLine:{
            normal:{
              length:2,
              length1:2
            }

          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1',
              color: function (value) {
                return getColorByCodeOrName(value.data.code)
              }
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
  return chart;
}

function showBar1(data){

  var chart = echarts.init(document.getElementById('bar1'));
  chart.setOption(
    {
      color: ['#FF8516'],
      tooltip: {
        trigger: 'axis'
      },
      grid: {
        bottom:'5%',
        left:'3%',
        right: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data:data.x,
          axisPointer: {
            type: 'shadow'
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }

      ],
      series: [
        {
          type: 'bar',
          data: data.data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}