var pie1Data = [
  {value: 51, name: 'A'},
  {value: 81, name: 'B'},
  {value: 15, name: 'C'},
  {value: 10, name: 'D'},
  {value: 10, name: '未定级'}
]

var bar1data = {
  x:['省级', '地市级', '区县级'],
  data:[
    [2.0, 4.9,1]
  ]
}
var line1data = {
  x:['1月', '秀洲区', '嘉善县', '海盐县', '海宁市', '平湖市', '桐乡市', '经开区', '嘉兴港区'],
  data:
    [1,2,3]

}
showPie1(pie1Data)
showBar1(bar1data);
showLine1(line1data);

function showPie1 (data) {
  var chart = echarts.init(document.getElementById('pie1'))
  chart.setOption(
    {
      color: [
        '#0099FF',
        '#21D376',
        '#F5A623',
        '#EB3432',
        '#A4BCC6'
      ],
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      series: [
        {
          name:'接触职业病危害因素占比分布',
          type: 'pie',
          radius: [25, 45],
          center: ['50%', '60%'],
          label: {
            normal: {
              show: true,
              position: 'outside',
              formatter: '{b}，{c}\n{d}%'
            }
          },
          itemStyle: {
            normal: {
              borderColor: '#fff',
              borderWidth: '1'
            }
          },
          data: data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}



function showBar1(data){

  var chart = echarts.init(document.getElementById('bar1'));
  chart.setOption(
    {
      color: [
        '#21D376',
        '#F5A623',
        '#EB3432'
      ],
      tooltip: {
        trigger: 'axis'
      },
      grid: {
        bottom:'5%',
        left:'3%',
        right: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data:data.x,
          axisPointer: {
            type: 'shadow'
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }
      ],
      yAxis: [
        {
          name:'单位：家',
          type: 'value',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          },
          nameTextStyle: {
            color: '#686868'
          }

        }
      ],
      series: [
        {
          type: 'bar',
          barWidth:'50',
          data: data.data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}
function showLine1(data){
  console.log(data)
  var chart = echarts.init(document.getElementById('line1'));
  chart.setOption(
    {
      color: [
        '#F5A623'
      ],
      tooltip: {
        trigger: 'axis'
      },
      grid: {
        bottom:'5%',
        left:'3%',
        right: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data:data.x,
          axisPointer: {
            type: 'shadow'
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          splitArea: {
            interval: 'auto',
            show: true,
            areaStyle: {
              color: ['#F8F8F8', '#fff']
            }
          },
          splitLine: {
            show: false
          },
          axisLine: {
            lineStyle: {
              color: '#1178C9'
            }
          },
          axisLabel: {
            color: '#686868'
          }

        }
      ],
      series: [
        {
          type: 'line',
          data: data.data
        }
      ]
    }
  )
  $(window).resize(function () {
      chart.resize()
    }
  )
}